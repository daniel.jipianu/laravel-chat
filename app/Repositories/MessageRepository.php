<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Message;
use Auth;

class MessageRepository
{

    public function sendMessage(Request $request)
    {
        $message = Auth::user()->messages()->create([
            'message' => $request->input('message')
        ]);

        return $message;
    }

}
