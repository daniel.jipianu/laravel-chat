<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\MessageRepository;
use App\Message;
use Auth;

class ChatController extends Controller
{
    private $messageRepository;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function getMessages()
    {
        return Message::with('user')->get();
    }

    public function sendMessage(Request $request)
    {
        $this->validate($request, [
            'message' => 'required|min:5'
        ]);

        $message = $this->messageRepository->sendMessage($request);

        return response()->json(['message' => $message]);
    }
}
